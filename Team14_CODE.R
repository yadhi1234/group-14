`ABNYC2019` <- read.csv("C:/Users/Yadgiri/Desktop/AB_NYC_2019.csv")

View(`ABNYC2019`)

mydata <- `3783168_1024589004_ABNYC2019`

library(skimr)

library(ggpubr)

mydat2 <- na.omit(mydata)

skim(mydat2$price)

skim(mydat2$number_of_reviews)

ggscatter(mydat2,x="number_of_reviews",y="price",
          
          add="reg.line",conf.int=TRUE,
          
          cor.coef=TRUE,cor.method="pearson",
          
          xlab="Number of Reviews",ylab="Price")

res <- cor.test(mydat2$price,mydat2$number_of_reviews,method="pearson")

res

fit <- lm(price~number_of_reviews,data = mydat2)

summary(fit)

savehistory("C:/Users/dennis.mwaniki/Desktop/re.Rhistory")